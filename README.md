InnoDrive Specs:
- Budet car price per minute = 25
- Luxury car price per minute = 56
- Fixed price per km = 11
- Allowed deviations in % = 10
- Inno discount in % = 14.000000000000002

Equivalence classes for each parameters:

| Parameter | Values |
| ------ | ------ |
| type | luxury, budget, nonsense |
| plan | minute, fixed_price, nonsense |
| distance | <=0, >0 |
| planned_distance | <=0, >0 |
| time | <=0, >0 |
| planned_time | 	<=0, >0 |
| inno_discount	| yes, no, nonsense |

 
First tests which I will conduct will be dedicated to the nonsense values. One should return `Invalid Request` in all these cases.  


| Test Case | Type | Plan | Distance | Planned Distance | Time | Planned Time	| InnoDiscount | Expected Result | Obtained |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| 1 | nonsense | minute | 10 | 10 | 10 | 10 | yes | Invalid | Invalid |
| 2	| luxury | nonsense | 10 | 10 | 10 | 10 | yes | Invalid | Invalid |
| 3 | luxury | minute | -1 | 10 | 10 | 10 | yes | Invalid | Invalid |
| 4 | luxury | minute | 10 | -1 | 10 | 10 | yes | Invalid | Invalid |
| 5 | luxury | minute | 10 | 10 | -1 | 10 | yes | Invalid | Invalid |
| 6 | luxury | minute | 10 | 10 | 10 | -1 | yes | Invalid | Invalid |
| 7 | luxury | minute | 10 | 10 | 10 | 10 | nonsense | Invalid | Invalid |

Ok, also let's check 0 and "null" values for all numerical parameters:

| Test Case | Type | Plan | Distance | Planned Distance | Time | Planned Time	| InnoDiscount | Expected Result | Obtained |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| 8 | budget | minute | 0 | 10 | 10 | 10 | yes | 250 | 250 |
| 9 | budget | minute | 10 | 0 | 10 | 10 | yes | 250 | 250 |
| 10 | budget | minute | 10 | 10 | 0 | 10 | yes | Invalid | 0 |
| 11 | budget | minute | 10 | 10 | 10 | 0 | yes | Invalid | 250 |
| 12 | budget | minute | null | 10 | 10 | 10 | yes | Invalid | 250 |
| 13 | budget | minute | 10 | null | 10 | 10 | yes | Invalid | 250 |
| 14 | budget | minute | 10 | 10 | null | 10 | yes | Invalid | null |
| 15 | budget | minute | 10 | 10 | 10 | null | yes | Invalid | null |

As we can see, the bugs are:
Service allows for "null" distances
Service allows for 0 and "null" planned distance
Service allows for 0 and "null" time
Service allows for 0 and "null" planned time
Good. Now let's concentrate on reasonable values.

| Test Case | Type | Plan | Distance | Planned Distance | Time | Planned Time	| InnoDiscount | Expected Result | Obtained |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| 16 | budget | minute | 10 | 100 | 10 | 100 | no | 250 | 250 |
| 17 | budget | fixed_price | 10 | 10 | 10 | 10 | no | 110 | 125 |
| 18 | luxury | minute | 10 | 10 | 10 | 10 | no | 560 | 560 |
| 19 | budget | fixed_price | 100 | 10 | 10 | 10 | no | 2500 | 166.66 |
| 20 | budget | fixed_price | 10 | 10 | 100 | 10 | no | 2500 | 166.66 |
| 21 | budget | fixed_price | 10 | 10 | 10 | 100 | no | 110 | 166.66 |

Fixed price plan works totally incorrect!!!
